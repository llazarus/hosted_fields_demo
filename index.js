const express = require('express');
const morgan = require('morgan');
const path = require('path');
const bodyParser = require('body-parser');
require('dotenv').config();

const app = express();

// View Engine
app.set('view engine', 'ejs');

// Middlewear
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));

// Router
const braintreeRouter = require('./routes/braintree');
app.use('/', braintreeRouter);

// Server
const DOMAIN = 'localhost';
const PORT = '5000';
app.listen(PORT, DOMAIN, () => {
  console.log(`Server is up and running on http://${DOMAIN}:${PORT}`);
});