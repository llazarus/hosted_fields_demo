const express = require('express');
const braintree = require('braintree');
const router = express.Router();

router.get('/', async (req, res) => {
  res.render('checkout')
});

const gateway = new braintree.BraintreeGateway({
  environment: braintree.Environment.Sandbox,
  merchantId: process.env.braintreeMerchantId,
  publicKey: process.env.braintreePublicKey,
  privateKey: process.env.braintreePrivateKey
});

router.get("/generate_client_token", (req, res) => {
  /* 
    The customerId parameter can optionally be set to allow returning customers to select from previously used payment method options.

    You do not need to include a customer id when creating a client token for a first time customers. Typically you would create create a customer when handling the submission of your checkout form, and then store that customer id in a database for use later. Since this demo does not have a connected database, a unique customerId will be generated for each transaction
  */

  // gateway.clientToken.generate({
  //   customerId: aCustomerId
  // }, (err, response) => {
  //   // pass clientToken to your front-end
  //   const clientToken = response.clientToken
  // });


  gateway.clientToken.generate({}, (err, response) => {
    res.send(response.clientToken);
  });
});

router.post('/', (req, res, next) => {
  // Use the payment method nonce here
  const nonceFromTheClient = req.body.paymentMethodNonce;
  const deviceDataFromTheClient = req.body.deviceData;

  // Create a new transaction for $15
  const txRequest = {
    amount: '15.00',
    // amount: '2001.00',
    paymentMethodNonce: nonceFromTheClient,
    deviceData: deviceDataFromTheClient,
    options: {
      // This option requests the funds from the transaction once it has been authorized successfully
      submitForSettlement: true
    }
  }
  
  gateway.transaction.sale(txRequest, (err, result) => {
    if (result) {
      console.log("Success! Transaction ID: " + result.transaction.id);
      res.send(result);
    } else {
      res.status(500).send(err);
    }
  });
});

router.post('/paypal-checkout', (req, res) => {
  // Use the payment method nonce here
  const nonceFromTheClient = req.body.paymentMethodNonce;
  const deviceDataFromTheClient = req.body.deviceData;
  const orderIdFromTheClient = req.body.orderId

  const saleRequest = {
    amount: '15.00',
    paymentMethodNonce: nonceFromTheClient,
    deviceData: deviceDataFromTheClient,
    // On PayPal transactions, this field maps to the PayPal invoice number. PayPal invoice numbers must be unique in your PayPal business account.
    orderId: orderIdFromTheClient,
    options: {
      submitForSettlement: true,
      paypal: {
        customField: "PayPal custom field",
        description: "Description for PayPal email receipt",
      },
    }
  };
  
  gateway.transaction.sale(saleRequest, (err, result) => {
    if (err) {
      console.log("Error:  " + err);
    } else if (result.success) {
      console.log("Success! Transaction ID: " + result.transaction.id);
      res.send(result);
    } else {
      console.log("Error:  " + result.message);
    }
  });
});

router.post('/void-tx', (req, res) => {
  const txIdFromTheClient = req.body.transactionId;

  gateway.transaction.void(txIdFromTheClient, (err, result) => {
    if (result) {
      if (result.success) {
        // transaction successfully voided
        res.send(result);
      } else {
        // check errors
        console.log(result.message);
      }
    } else {
      console.log('Transaction not found!');
      res.send(false);
    }
  });
});


module.exports = router;