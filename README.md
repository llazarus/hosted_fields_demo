# Braintree Hosted Fields Demo App

To run this application (assuming node.js is already installed):

*  Clone the repository: ```git clone https://llazarus@bitbucket.org/llazarus/hosted_fields_demo.git```  
*  Cd into the project directory: ```cd hosted_fields_demo```
*  Within /hosted_fields_demo you will need to create a .env file to store the merchantId, publicKey, and privateKey: ```touch .env```
    *  Open the .env file and set brainTreeMerchantId, braintreePublicKey, and braintreePrivateKey with the values obtained from the API dashboard:
```
braintreeMerchantId=VALUE_FROM_DASHBOARD
braintreePublicKey=VALUE_FROM_DASHBOARD
braintreePrivateKey=VALUE_FROM_DASHBOARD
```
*  Install package dependencies: ```npm install```
*  Start the dev server: ```npm run start```
*  Open the application in your browser: http://localhost:5000